const products = [];

module.exports = {
    Query: {
        product: (_, { id }) => products[id],
        products: () => products
    },
    Mutation: {
        addProduct: (_, { name, qte }) => {
            const product = { id: products.length, name, qte };
            products.push(product);
            return product;
        }
    }
};
