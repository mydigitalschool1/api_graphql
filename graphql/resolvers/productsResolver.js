const fetch = require('node-fetch');

//const baseURL = 'https://world.openfoodfacts.org/api/v2/';
const baseURL = 'https://world.openfoodfacts.org/api/v0/product/';
const searchQueryKey = '?s=';
const productIdQueryKey = '?i=';
//const yearOptionalQueryKey = '&y=';
//const apiKey = '&apikey=f229262c';

const omdbFetcher = args => {
    const fullURL = args.productCode
        ? findByproductCode(args.productCode)
        : searchFor(args.productInput.code);

    return fetch(fullURL)
        .then(response => response.json())
        .catch(error => {
            throw new Error(error.Error)
        });
};

const findByproductCode = code => baseURL + code;

const searchFor = (code) => {
    return code ?
        baseURL + code :
        baseURL + code;
};

module.exports = {
    products: async args => {
        try {
            const products = await omdbFetcher(args);
            return products.Search;
        } catch (e) {
            console.log(e);
        }
    },
    product: async args => {
        try {
            const product = await omdbFetcher(args);
            return product;
        } catch (e) {
            console.log(e);
        }
    }
};