const movieResolver = require('./movieResolver');
const productsResolver = require('./productsResolver');

// rootResolver est un objet qui fait référence au contenu de notre movieResolver
const rootResolver = {
    ...productsResolver
    // On ajoutera ici d'autres resolvers au besoin
};

module.exports = rootResolver;