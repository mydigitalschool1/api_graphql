/*
/!*const { gql } = require('express-graphql');

module.exports = gql`

    type ProductType {
        id: Int!
        name: String!
        code: String!
        qte: Int
    }

    #extend type Mutation {
    #    register(input: RegisterInput!): RegisterResponse
    #    login(input: LoginInput!): LoginResponse
    #}

    input ProductInput {
        name: String!
        qte: Int
    }

    type RootQuery {
        products(ProductInput: ProductInput): [ProductType!]!
        products(productId: Int!): ProductType!
    }

    schema {
        query: RootQuery
    }

`;*!/

/!*type RegisterResponse {
    id: Int!
        name: String!
        qte: Int!
}

input RegisterInput {
    id: Int!
        name: String!
        qte: Int
}

input LoginInput {
    name: String!
        qte: Int
}

type LoginResponse {
    id: Int!
        name: String!
        qte: Int!
}*!/

const { buildSchema } = require('graphql');

module.exports = buildSchema(`
  """
  A MovieType refers to available attributes for Movie
  """
  type ProductType {
    "Product ID from open food api"
    id: ID!
    "name of product"
    name: String!
    "code of product"
    code: String!
    "Available Quantity"
    qte: Int
  }
  input ProductInput {
        name: String!
        qte: Int
    }
  type RootQuery {
        products(ProductInput: ProductInput): [ProductType!]!
        products(productId: Int!): ProductType!
    }
  schema {
    query: RootQuery
  }
`);
*/
