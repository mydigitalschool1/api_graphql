const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const graphql = require('graphql');
const {buildSchema, GraphQLSchema, GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList} = require('graphql');
const bodyParser = require('body-parser');
const productsRouter = require('./routes/products.js');

// Import du schema et du point d'entrée des resolvers
const graphQlSchema2 = require('./graphql/schema/productSchema');
const graphQlResolvers = require('./graphql/resolvers/index');

//swagger for documentation
/*const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');*/

// Construct a schema, using GraphQL schema language
/*var schema = buildSchema(`
  type Query {
    ping: String,
    hello: String,
  }
`);*/

app.use(bodyParser.json());

app.use(
    '/graphql2',
    graphqlHTTP({
        schema: graphQlSchema2,
        rootValue: graphQlResolvers,
        graphiql: true
    })
);


const ProductTypeTest = new GraphQLObjectType({
    name: "ProductTest",
    fields: () => ({
        id: {type: GraphQLInt},
        code: {type: GraphQLString},
        name: {type: GraphQLString},
        qte: {type: GraphQLInt},
    })
})

const MovieType = buildSchema(`
  """
  MovieType attributes definition
  """
  type MovieType {
    "Title of a movie"
    title: String!
    "Release year"
    year: Int!
  }
`);

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: () => ({
        getAllProducts: {
            type: new GraphQLList(ProductTypeTest),
            args: {id: {type: GraphQLInt}},
            resolve(parent, args) {
                //return "productData";
                return {name};
            }
        }
    })
})
const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        createProduct: {
            type: ProductTypeTest,
            args: {
                name: {type: GraphQLString},
                qte: {type: GraphQLInt}
            },
            resolve(parent, args) {
                //productData.push({id: 4, name: "Commode", qte: 4})
                //add in database
                return args;
            }
        }
    }
})

const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
})

// The root provides a resolver function for each API endpoint
var root = {
    ping: () => {
        return 'pong';
    },
    hello: () => {
        return 'Hello world!';
    },
};

const app = express();
/*app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));*/

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: {
        getProduct: (args) => {
            return new Promise((resolve, reject) => {
                const options = {
                    hostname: 'world.openfoodfacts.org',
                    path: `/api/v0/product/${args.code}.json`,
                    method: 'GET'
                };

                https.get(options, (res) => {
                    let data = '';
                    res.on('data', (chunk) => {
                        data += chunk;
                    });

                    res.on('end', () => {
                        resolve(JSON.parse(data));
                    });
                }).on('error', (e) => {
                    reject(e);
                });
            });
        }
    },
    graphiql: true
}));

app.get('/', (req, res) => {
    //init database if necessary
    //productController.getAll;
    //res.sendFile('./views/index.html');
    res.sendFile(__dirname + '/views/index.html');
});

app.use('/api', productsRouter);

//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const port = 3001;

app.listen(port, ()=>{
    console.log('server is up !');
});

module.exports = app;