const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../models/database.js");
const Product = require('../models/product.model.js');

exports.getAll = (req, res, next) => {
    Product.findAll().then(product => {
        if(product.length < 0){
            Product.bulkCreate([
                { name: "Table", qte: 2 },
                { name: "Chaise", qte: 6},
                { name: "Armoire", qte: 11 },
            ]).then(() => console.log("Products have been saved")); 
        }else{
        res.json(product);
        }
    }).catch(next);
};

exports.get = async (req, res, next) => {
    const id = req.params.productId;
    const product = await Product.findByPk(id)
        .catch(next);
    if(product) {
        res.json(product);
    } else {
        res.status(404).send(`User with id ${id} not found!`);
    }
};

exports.create = (req, res, next) => {
    const newPrd = req.body;
    let prdList = [];
    //avoid name duplicates
    Product.findAll().then(async (prd) => {
        prd.forEach(element => prdList.push(element.name.toLowerCase()));
        if(!prdList.includes(newPrd.name.toLowerCase())){
            Product.create(req.body).then(p => res.json(p))
            .catch(next);
        }else{
            res.status(400).send("Un produit avec la même dénomination existe déjà.");
        }
    }).catch(next);    
};

exports.update = (req, res, next) => {
    const newPrd = req.body;
    const id = req.params.productId;
    //list of products
    Product.findAll().then(async (prd) => {
        prd.forEach(element => prdList.push(element.name));
        if(!prdList.includes(newPrd.name)){
            Product.findByPk(id).then(async (prd) => {
                if (prd) {
                    //prevent id change
                    newPrd.id = prd.id;
                    await Product.update(newPrd, {where: {id}})
                    res.status(200).send("Produit modifié avec succès !");
                }else {
                    res.status(404).send();
                }
            }).catch(next);
        }else{
            res.status(400).send("Un produit avec la même dénomination existe déjà.");
        }
    }).catch(next);
};

exports.updateStock = (req, res, next) => {
    const id = req.params.productId;
    const newQte = req.body.qte;
    Product.findByPk(id).then(async (prd) => {
        if (prd) {
            //change only quantity
            await Product.update({ qte : newQte }, {where: {id}});
            res.status(200).send("Stock mis à jour avec succès !");
        }else {
            res.status(404).send();
        }
    }).catch(next);
}

exports.updateStockAdd = (req, res, next) => {
    const id = req.body.id;
    let newQte = req.body.qte;
    if( !isNaN(newQte) && newQte > 0){
        Product.findByPk(id).then(async (prd) => {
            if (prd) {
                //change only quantity
                newQte = prd.qte + newQte;
                await Product.update({ qte : newQte }, {where: {id}});
                res.status(200).send("Stock mis à jour avec succès !");
            }else {
                res.status(404).send("can't find product.");
            }
        }).catch(next);
    }else{
        res.status(404).send("Stock must be a positive integer.");
    }
}

exports.updateStockMinus = (req, res, next) => {
    const id = req.body.id;
    let newQte = req.body.qte;
    if( !isNaN(newQte) && newQte > 0){
        Product.findByPk(id).then(async (prd) => {
            if (prd) {
                //change only quantity
                newQte = prd.qte - newQte;
                if(newQte < 0) newQte = 0;
                await Product.update({ qte : newQte }, {where: {id}});
                res.status(200).send("Stock mis à jour avec succès !");
            }else {
                res.status(404).send("can't find product.");
            }
            res.status(404).send("id = "+id);
        }).catch(next);
    }else{
        res.status(404).send("Stock must be a positive integer.");
    }
}

exports.deleteProduct = (req, res, next) => {
    const id = req.params.productId;
    Product.destroy({where:{id}}).then(res.status(200).send("Produit supprimé avec succès !")).catch(next);
};

//TODO if database is empty = create data

//sequelize.sync().then(() => {

//    exports.getAll = (req, res, next) => {
//        Product.findAll().then(product => {
//            res.json(product);
//        }).catch(next);
//    };

/*    Product.findAll().then(res => {
        res.json(res);
        //console.log(res)
    }).catch((error) => {
        console.error('Failed to retrieve data : ', error);
    });

    Product.findOne({
        where: {
            id : "1"
        }
    }).then(res => {
        console.log(res)
    }).catch((error) => {
        console.error('Failed to retrieve data : ', error);
    });*/

//}).catch((error) => {
//    console.error('Unable to create table : ', error);
//});

