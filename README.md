# Api_GraphQL



## Getting started

In order to use it, here are the request and parameters to interact with the apis to get products data.
URL to GraphQL interface: http://localhost:3002/graphql

Query: 
- allProducts: get all the products in database.
- getByCodeProduct: get the product name by its barcode. Variable = code (String)!
- getCodeProductByName: get the product code by its product_name. Variable = product_name (String)!

Mutation:
- createProduct: create a product in stockApi database. Variable = code (String), name (String)!, qte (Int)
- createProductWithCode: create a product in stockApi database with Openfoodfact data. Variable = code (String)!, name (String)!, qte (Int)
- updateProduct: update a product in stockApi database. Variable = id (Int)!, name (String), code (String), qte (Int)
- deleteProduct: delete a product in stockApi database. Variable = id (Int)!

## Description
This project use mds_api_rest project and Openfoodfact api.
