const express = require('express');
const { graphqlHTTP } = require('express-graphql');
//const { makeExecutableSchema } = require("graphql-tools");
const resolvers = require("./graphql/resolvers/productSchemaResolver");
const graphQlSchema2 = require("./graphql/schema/productSchema");
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
//const fs = require("fs");
//const typeDefs = fs.readFileSync("./typeDefs.gql", "utf8");
//const sofa = require("sofa-api");
//const schemaRest = makeExecutableSchema({ typeDefs, resolvers });
const {
    buildSchema,
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull} = require('graphql');

const options = {
    definition: {
        info: {
            title: "mds_api_qraphql",
            version: "1.0.0",
            description: "QraphQl Api using 2 apiStock and OpenFoodFact api"
        },
        servers: [
            {
                url: "http://localhost:3002"
            }
        ]
    },
    apis: ["./app.js"]
}

const specs = swaggerJsDoc(options);

const app = express();

const PORT = 3002;

var products = [
    {id: 1, code: "Code", prdname: "Table", name:"Table", qte: 2},
    {id: 2, code: "Code", prdname: "Chaise", name: "Chaise", qte: 6},
    {id: 3, code: "Code", prdname: "Armoire", name: "Armoire", qte: 11},
]

/*const one_prd = fetch('https://world.openfoodfacts.org/api/v0/product/7622210449283', { //code: 7622210449283
    method: 'GET',
})
    .then((res) => res.json())
    .then((result) => console.log(result))*/

require('isomorphic-fetch');

/*fetch('https://world.openfoodfacts.org/api/v0/product/7622210449283', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query: `
    query {
      id {
        product_name 
	    }
      }
    }`
    }),
})
    .then(res => res.json())
    .then(res => console.log(res.data));*/

const BASE_URL_MDSREST = "http://localhost:3000/api";
const BASE_URL_APIFOOD = "https://world.openfoodfacts.org/api/v2";

const api_rest_prd = fetch('http://localhost:3000/api/products', { //code: 7622210449283
    method: 'GET',
})
    .then((res) => res.json())
    //.then((result) => console.log(result))

const ProductTypeTest = new GraphQLObjectType({
    name: "ProductTest",
    fields: () => ({
        id: {type: GraphQLInt},
        name: {type: GraphQLString},
        code: {type: GraphQLString},
        qte: {type: GraphQLInt},
        createdAt: {type: GraphQLString},
        updatedAt: {type: GraphQLString},
    })
})

const ProductTypeApiFood = new GraphQLObjectType({
    name: "ProductTypeApiFood",
    fields: () => ({
        code: {type: GraphQLString},
        product_name: {type: GraphQLString}
    })
})

/**
 * @swagger
 * get requests
 */
const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: () => ({
        getAllProducts: {
            type: new GraphQLList(ProductTypeTest),
            args: {id: {type: GraphQLInt}},
            resolve(parent, args) {
                //return "productData";
                return api_rest_prd;
            }
        }
    })
})

const QueryType = new GraphQLObjectType({
    name: 'Query',
    description: 'The root of all... queries',
    fields: () => ({
        allProducts: {
            type: new GraphQLList(ProductTypeTest),
            resolve: root => api_rest_prd// Fetch the index of people from the REST API,
        },
        product: {
            type: ProductTypeTest,
            args: {
                id: { type: GraphQLString },
            },
            resolve: (root, args) => fetch(`${BASE_URL_MDSREST}/products/${args.id}`)
                .then((res) => res.json())
                .then(json => json)// Fetch the person with ID `args.id`,
        },
        getByCodeProduct: {
            type: ProductTypeApiFood,
            args: {
                code: { type: GraphQLString },
            },
            resolve: (root, args) => fetch(`${BASE_URL_APIFOOD}/search?code=${args.code}&fields=code,product_name`)
                .then((res) => res.json())
                .then(json => json.products[0])// Fetch the person with ID `args.code`,
            //https://world.openfoodfacts.org/api/v2/search?code=8024884500403,3263855093192&fields=code,product_name
        },
        getCodeProductByName: {
            type: ProductTypeApiFood,
            args: {
                product_name: { type: GraphQLString },
            },
            resolve: (root, args) => fetch(`${BASE_URL_APIFOOD}/search?code=${args.product_name}&fields=code,product_name`)
                .then((res) => res.json())
                .then(json => json.products[0])// Fetch the person with ID `args.code`,
            //https://world.openfoodfacts.org/api/v2/search?code=8024884500403,3263855093192&fields=code,product_name
        },
    }),
});

//https://graphql.org/graphql-js/mutations-and-input-types/
const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        createProduct: {
            type: ProductTypeTest,
            args: {
                code: {type: GraphQLString},
                name: {type: GraphQLString},
                qte: {type: GraphQLInt}
            },
            resolve: (root, args) => /*fetch(`${BASE_URL_MDSREST}/products/${args.id}`)
                .then((res) => res.json())
                .then(json => json)*/// Fetch the person with ID `args.id`,
            fetch(`${BASE_URL_MDSREST}/products`, {
            method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({name: `${args.name}`, code: `${args.code}`, qte: `${args.qte}`})
            }).then((res) => res.json())
        },
        createProductWithCode: {
            type: ProductTypeTest,
            args: {
                code: {type: GraphQLString},
                name: {type: GraphQLString},
                qte: {type: GraphQLInt}
            },
            resolve: (root, args) =>
                fetch(`${BASE_URL_APIFOOD}/search?code=${args.code}&fields=code,product_name`)
                    .then((res) => res.json())
                    .then((json) => json.products[0])
                    .then(json => fetch(`${BASE_URL_MDSREST}/products`, {
                        method: 'POST',
                        headers: {
                            'Accept': '*/*',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({name: `${json.product_name}`, code: `${json.code}`, qte: `${args.qte}`})
                    }).then((res) => res.json()))
        },
        updateProduct: {
            type: ProductTypeTest,
            args: {
                id: {type: GraphQLInt},
                name: {type: GraphQLString},
                code: {type: GraphQLString},
                qte: {type: GraphQLInt}
            },
            resolve: (root, args) =>
                fetch(`${BASE_URL_MDSREST}/products/${args.id}`, {
                    method: 'PUT',
                    headers: {
                        'Accept': '*/*',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({name: `${args.name}`, code: `${args.code}`, qte: `${args.qte}`})
                }).then((res) => res)
        },
        deleteProduct: {
            type: ProductTypeTest,
            args: {
                id: {type: GraphQLInt}
            },
            resolve: (root, args) => fetch(`${BASE_URL_MDSREST}/products/${args.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Accept': '*/*',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({name: `${args.name}`, qte: `${args.qte}`})
                }).then((res) => res)
        }
    }
})

const schema = new GraphQLSchema({
    query: QueryType,
    mutation: Mutation,
})

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
}));

//app.use("/rest", sofa({ schemaRest }));

app.listen(PORT, ()=>{
    console.log('server is up !');
});

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

module.exports = app;