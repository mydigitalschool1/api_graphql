const router = require('express').Router();
//const productController = require('../controllers/product.controller.js');

//http://localhost:3000/api/products

/*router.route('/products').get(
    fetch('https://www.learnwithjason.dev/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: `
        query GetLearnWithJasonEpisodes($now: DateTime!) {
          allEpisode(limit: 10, sort: {date: ASC}, where: {date: {gte: $now}}) {
            date
            title
            guest {
              name
              twitter
            }
            description
          }
        }
      `,
            variables: {
                now: new Date().toISOString(),
            },
        }),
    })
        .then((res) => res.json())
        .then((result) => console.log(result))
);*/

fetch('https://www.learnwithjason.dev/graphql', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        query: `
        query GetLearnWithJasonEpisodes($now: DateTime!) {
          allEpisode(limit: 10, sort: {date: ASC}, where: {date: {gte: $now}}) {
            date
            title
            guest {
              name
              twitter
            }
            description
          }
        }
      `,
        variables: {
            now: new Date().toISOString(),
        },
    }),
})
    .then((res) => res.json())
    .then((result) => console.log(result));

fetch('http://localhost:3000/api/products', {
    method: 'GET',
})
    .then((res) => res.json())
    .then((result) => console.log(result))

const https = require('https');

const options = {
    hostname: 'world.openfoodfacts.org',
    //path: '/api/v0/product/[CODE_PRODUIT].json',
    path: '/api/v0/product/7622210449283.json',
    method: 'GET'
};

https.get(options, (res) => {
    console.log(`Statut de la réponse: ${res.statusCode}`);

    res.on('data', (d) => {
        // Traiter les données retournées par l'API ici
        //console.log(JSON.stringify(d));
    });
}).on('error', (e) => {
    console.error(e);
});



module.exports = router;