/*const mysql = require('mysql');
//connect with database
export default pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'mds_api_rest'
})*/

// Include Sequelize module
const Sequelize = require('sequelize')
const Product = require('./product.model.js');

// Creating new Object of Sequelize
const sequelize = new Sequelize(
    'mds_api_graphql', //db name
    'root', //username
    '', //password
     {
       host: 'localhost',
       dialect: 'mysql'
     }
   );

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
 }).catch((error) => {
    console.error('Unable to connect to the database: ', error);
 });

//init db
sequelize.query('CREATE DATABASE IF NOT EXISTS mds_api_graphql;').then(() => {
   Product.findAll().then(product => {
      if(product.length <= 0){
          Product.bulkCreate([
              { name: "Table", qte: 2 },
              { name: "Chaise", qte: 6},
              { name: "Armoire", qte: 11 },
          ]).then(() => console.log("Products have been saved")); 
      }else{
         console.log('Connect to db');
      }
  });
})

 /*const models = [
    // Add here all of your models
    require('./product.model'),
   ].map(m=>m(sequelize));*/

sequelize.sync().then(console.log('DB is synced'));

// Exporting the sequelize object.
// We can use it in another file
// for creating models
module.exports = sequelize