const { Sequelize, DataTypes } = require("sequelize");
//const sequelize = new Sequelize("mysql::memory:");
//import sequelize from "../server.js";

const sequelize = new Sequelize(
    'mds_api_graphql', //db name
    'root', //username
    '', //password
     {
       host: 'localhost',
       dialect: 'mysql'
     }
);
   
/*sequelize.authenticate().then(() => {
       console.log('Connection has been established successfully.');
    }).catch((error) => {
       console.error('Unable to connect to the database: ', error);
});*/

const Product = sequelize.define("products", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    code: {
        type: DataTypes.STRING,
        allowNull: true
    },
    qte: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
 });

 sequelize.sync().then(() => {
    console.log('Product table created successfully!');
 }).catch((error) => {
    console.error('Unable to create table : ', error);
 });

 module.exports = Product